import React, {useState} from "react";
import './style.css';
import SupportBox from '../sections/supportBox'
import SupportButton from '../sections/supportButton'


function Main() {
    const [status, setStatus]=useState(false)
    return (
        <div className="main">
            {status
                ? <SupportBox onClose={setStatus}/>
                : <SupportButton onClick={setStatus}/>
            }
        </div>
    );
}
export default Main;