import {close, message, send} from "../components/icon/svg";

const iconPath = (name, stroke, strokeWidth) => {
    let icon =
        name === 'message' && message(stroke, strokeWidth) ||
        name === 'close' && close(stroke, strokeWidth) ||
        name === 'send' && send(stroke, strokeWidth)
    return icon
}
export default iconPath