import React from "react";
import iconPath from "../../utils/icon";

function Icon ({
   name,
   _class= null,
   width,
   height,
   viewBox="0 0 24 24",
   fill='none',
   stroke,
   strokeWidth
}){
    return (
        <svg
        className={_class}
        width={width}
        height={height}
        viewBox={viewBox}
        fill={fill}
        >
            {iconPath(name, stroke, strokeWidth)}
        </svg>
    )
}
export default Icon;

