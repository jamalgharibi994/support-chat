import React from "react";
import './style.css';
import Icon from "../../../icon";

function CloseButton ({onClose, animStat}) {
    return (
        <div className="icon-close" onClick={()=> {
            animStat(false)
            setTimeout(()=>{onClose(false)},500)
        }} >
            <Icon name={"close"} width={"24"} height={"24"} stroke={"#ffffff"} />
        </div>
    )
}
export default CloseButton;