import React from "react";
import './style.css';
import CloseButton from "./component/closeButton";


function Header({onClose, animStat}) {
    return (
        <header className="header">
            <CloseButton onClose={onClose} animStat={animStat}/>
            <p className="welcome">به گفتگوی آنلاین خوش آمدید</p>
        </header>
    );
}

export default Header;
