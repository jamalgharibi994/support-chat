import React from "react";
import "./style.css";

function InputTooltip({ tooltip, setSelectedTooltip }) {

    const handleKeyDown = (event, name) => {
        if (event.key === "ArrowUp") {
            event.preventDefault();
            const buttons = document.querySelectorAll(".tooltip");
            const index = Array.from(buttons).findIndex(
                (button) => button.textContent === name
            );

            if (index > 0) {
                buttons[index - 1].focus();
            }
        } else if (event.key === "ArrowDown") {
            event.preventDefault();
            const buttons = document.querySelectorAll(".tooltip");
            const index = Array.from(buttons).findIndex(
                (button) => button.textContent === name
            );
            if (index < buttons.length - 1) {
                buttons[index + 1].focus();
            }
        } else if (event.key === "Enter") {
            event.preventDefault();
            setSelectedTooltip(name);
        }
    }
    const onClickHandler =(name) => {
        setSelectedTooltip(name);
    }

    return (
        <>
            {tooltip.symbol === "@" ? (
                <div className="tooltips">
                    <button
                        className="tooltip"
                        tabIndex="0"
                        onKeyDown={(event) => handleKeyDown(event, "جمال")}
                        onClick={() => onClickHandler("جمال")}
                    >
                        جمال
                    </button>

                    <button
                        className="tooltip"
                        tabIndex="0"
                        onKeyDown={(event) => handleKeyDown(event, "پوریا")}
                        onClick={() => onClickHandler("پوریا")}
                    >
                        پوریا
                    </button>

                    <button
                        className="tooltip"
                        tabIndex="0"
                        autoFocus
                        onKeyDown={(event) => handleKeyDown(event, "علی")}
                        onClick={() => onClickHandler("علی")}
                    >
                        علی
                    </button>
                </div>
            ) : (
                <div className="tooltips">
                    <button
                        className="tooltip"
                        tabIndex="0"
                        onKeyDown={(event) => handleKeyDown(event, "بن")}
                        onClick={() => onClickHandler("بن")}
                    >
                        بن
                    </button>

                    <button
                        className="tooltip"
                        tabIndex="0"
                        autoFocus
                        onKeyDown={(event) => handleKeyDown(event, "آن بن")}
                        onClick={() => onClickHandler("آن بن")}
                    >
                        آن بن
                    </button>
                </div>
            )}
        </>
    );
}

export default InputTooltip;

