import React, {useEffect, useState} from "react";
import './style.css';
import Icon from "../../../icon";

function MessageInput(props) {

    const [valueTooltip ,setValueTooltip] = useState("")
    const [lastSignStat ,setLastSignStat] = useState(false)

    const initialTooltipVal ={status: false,symbol: null}

    const handleTooltip = (symbol) =>{
        props.setTooltip({
            status: true,
            symbol: symbol
        })
    };

    // for get input value && Terms related to Tooltip
    const handleInput = event => {

        let valLength = event.target.value.length
        let lastChar = event.target.value.charAt(valLength - 1)
        let signs = (lastChar === '/' || lastChar === '@') && lastChar
        let indexSlash = event.target.value.indexOf('/')
        let indexAtSign = event.target.value.indexOf('@')

        if (signs) handleTooltip(signs)

        if (!signs) {
            props.setTooltip(initialTooltipVal)
        }

        if (
            (indexSlash !== -1 &&
            event.target.value.charAt(indexSlash + 1) ||
            indexAtSign !== -1 &&
            event.target.value.charAt(indexAtSign + 1)) &&
            !lastSignStat
        )
            props.setTooltip(initialTooltipVal)
    };

    // To avoid reloading the page
    const handleSubmit = (event) => {
        event.preventDefault();
        if (event.target[0].value === "")
            return
        props.setMessages([...props.messages, { message: event.target[0].value }]);
        document.getElementById('input').value=""
    };

    // To send a message with Inter
    const pressEnter = event => {
        if (event.target.value === '')
            return
        if (event.key === "Enter") {
            props.setMessages([...props.messages, {message: event.target.value}])
            document.getElementById('input').value=""
        }
    };

    const botHandler = () =>{
        if(props.tooltipValue){
            setValueTooltip(props.tooltipValue)
            props.setSelectedTooltip('')
            props.setTooltip(initialTooltipVal)
            let inputVal = document.getElementById('input').value
            document.getElementById('input').value = `${inputVal}${props.tooltipValue} `
            setLastSignStat(true)
        }
    }

    useEffect(
        ()=> {
            botHandler()
            document.getElementById('input').focus()
        }, [valueTooltip, props.tooltipValue]
    )

    return (
        <>
            <form className="inputBox" onSubmit={handleSubmit}>
                <input id="input"
                       className="input"
                       name="input"
                       type="text"
                       autocomplete="off"
                       placeholder="پیام شما ..."
                       onChange={handleInput}
                       onKeyDown={pressEnter}
                       autoFocus
                />
                <button className="icon-send" type="submit">
                    <Icon name={"send"} width={"24"} height={"24"} stroke={"rgba(2,2,2,0.80)"} />
                </button>
            </form>
        </>

    );
}
export default MessageInput;
