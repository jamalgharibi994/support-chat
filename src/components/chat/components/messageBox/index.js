import React, { useEffect, useRef} from "react";
import './style.css';
import avatar from '../../../../public/avatar/avatar.png';


function MessageBox({messages}) {

    // for auto scroll
    const Ref = useRef(null);
    const scrollToBottom = () => {
        Ref.current?.scrollIntoView({behavior:'smooth'})
    };
    useEffect(() => {
        scrollToBottom()
    });


    return (
       <>
           <section className="messages">
               {messages.map((item) => {
                   return <div className="message-box">
                               <div className="profile-box">
                                   <img className="avatar" src={avatar} />
                                   <span className="user">user :</span>
                               </div>
                               <div className="message">{item.message}</div>
                        </div>
               })}
               {/*for auto scroll*/}
               <div ref={Ref}/>
           </section>
       </>
    );
}
export default MessageBox;