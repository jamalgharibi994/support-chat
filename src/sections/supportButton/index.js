import React from "react";
import Icon from "../../components/icon";
import './style.css';

function SupportButton ({onClick}) {
    return (
        <div className="icon-message" onClick={()=> onClick(true)} >
            <Icon name={"message"} width={"40"} height={"40"} stroke={"#ffffff"} />
        </div>
    )
}
export default SupportButton;