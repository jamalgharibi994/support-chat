import React, {useState} from "react";
import './style.css'

import Header from "../../components/header";
import MessageBox from "../../components/chat/components/messageBox";
import MessageInput from "../../components/chat/components/messageInput";
import InputTooltip from "../../components/chat/components/messageInput/component/inputTooltip";

function SupportBox ({onClose}) {
    const initMessages = [
        {
            'message': 'سلام چطوری می تونم کمکتون کنم؟'
        }
    ]
    const [animStat, setAminStat]= useState(true)
    const [messages, setMessages] = useState(initMessages)
    const [tooltip , setTooltip] = useState({
        status: false,
        symbol: null
    })
    const [selectedTooltip, setSelectedTooltip] = useState("");
    return (
        <section className={`chatBox ${animStat ? 'animationOpen' : 'animationClose'}`}>
            <Header onClose={onClose} animStat={setAminStat} />
            <MessageBox messages={messages} />

            {tooltip.status && <InputTooltip tooltip={tooltip} setSelectedTooltip={setSelectedTooltip} />}

            <MessageInput setMessages={setMessages}
                          messages={messages}
                          setTooltip={setTooltip}
                          tooltipValue={selectedTooltip}
                          setSelectedTooltip={setSelectedTooltip}
                          tooltip={tooltip}
                 />
        </section>
    )
}
export default SupportBox;
